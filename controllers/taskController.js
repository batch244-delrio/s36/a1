// controller - bussiness logic

const Task = require("../models/task");

module.exports.getAllTask = () => {
	return Task.find({}).then (result => {
		return result;
	});
};

// creating tasks
module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
		name : requestBody.name
	})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
};

// deleting tasks
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

// updating task
module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error);
			return false;

		} else {
			result.name = newContent.name;
			return result.save().then((updatedTask, saveErr) => {
				if (saveErr){
					console.log(saveErr)
					return false;
				} else {
					return updatedTask;
				}
			})
		}
	})
}

// Activity-------------------------------------------------

//  Create a controller function for changing the status of a task to "complete".
module.exports.updateStat = (taskId, newCont) => {

	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error);
			return false;

		} else {
			result.status = "complete";
			return result.save().then((updatedStat, saveErr) => {
				if (saveErr){
					console.log(saveErr)
					return false;
				} else {
					return updatedStat;
				}
			})
		}
	})
}


module.exports.getSpecificTask = (taskId) => {

	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error);
			return false
		} else {
			return result.save()
		}
	})

}


