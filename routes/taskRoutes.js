// routes - conttains all the endpoints for our application

const express = require("express")
const router = express.Router();
const taskController = require("../controllers/taskController");

// route to all get task
router.get("/", (req, res) =>{
	taskController.getAllTask().then(resultFromController => res.send(resultFromController));
});

// route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// route to delete a task
// ":id" is a wildcard
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
		//params - URL parameter
});

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});



// ACTIVITY -----------------------------------------------------

//  Create a route for changing the status of a task to "complete".

router.put("/:id/complete", (req, res) => {
	taskController.updateStat(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/:id", (req, res) =>{
	taskController.getSpecificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});















// export the module 
module.exports = router;